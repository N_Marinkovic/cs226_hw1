/**
 *SquareCounter implements the ResetableCounter interface. It
 *increments and decrements by taking the square and square 
 *root of its current value, respectively.
 *
 *@author Nevena Marinkovic (nmarink1@jhu.edu)
 *@since 2016-09-11
 */

public class SquareCounter implements ResetableCounter{
    /** Private integer variable storing the current value
     *of the counter, initialized to 2 in the constructor. */
    private int value = 2;

    @Override
    public int value() {
	return this.value;
    }
    
    /** Increments value by squaring it. */
    public void up() {
	this.value = (this.value)*(this.value);
    }

    /** Decrements value by taking its square root. If its
     *root is not an integer it is rounded to its ceiling.
     *(eg. 1.1, 1.5, and 1.9 are all rounded to 2.)
     */
    public void down() {
	this.value = (int)Math.ceil(Math.sqrt(this.value));
    }

    /** Resets value to 2. */
    public void reset() {
	this.value = 2;
    }

    /** This is the main method containing assert statements
     *testing for the methods in the SquareCounter class. */
    public static void main(String[] args) {
	ResetableCounter c = new SquareCounter();

	assert(c.value() == 2);
	c.up();
	c.up();
	assert(c.value() == 16);
	c.down();
	c.down();
	c.down();
	assert(c.value() == 2);
	c.reset();
	assert(c.value() == 2);
    }
}
