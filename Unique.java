/**
 *The Unique class takes in any number of integers from the command
 *line and prints them each on a seperate line.
 *
 *@author Nevena Marinkovic (nmarink1@jhu.edu(
 *@since 2016-09-11
 */

public class Unique {

    public static void main(String[] args) {

        int n = args.length;
	
	for (int i=0; i<n; i++) {
	    System.out.println(args[i]);
	}

	System.out.println();
    }
}
