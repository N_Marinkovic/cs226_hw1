/**
*BasicCounter implements the ResetableCounter interface, it 
*increments and decrements the counter by 1.
*
*@author Nevena Marinkovic (nmarink1@jhu.edu)
*@since 2016-09-11
*/

public class BasicCounter implements ResetableCounter{
    /** Private integer variable storing the current value
     *of the counter, initialized to 0 in the constructor. */
    private int value;
    
    @Override
    public int value() {
	return this.value;
    }
    
    /** Increments value up by 1. */
    public void up() {
	this.value += 1;
    }

    /** Decrements value down by 1. */
    public void down() {
	this.value -= 1;
    }

    /** Resets value to 0.*/
    public void reset() {
	this.value = 0;
    }

    /** This is the main method containing assert statements
     *testing for the methods in the BasicCounter class. */
    public static void main(String[] args) {
	ResetableCounter c = new BasicCounter();

	assert(c.value() == 0);
	c.up();
	c.up();
	assert(c.value() == 2);
	c.down();
	assert(c.value() == 1);
	assert(c.value() == 1);
        c.reset();
	assert(c.value() == 0);
    }
}
