
/**
*An extension of the Counter interface which adds a method 
*to reset the counter to it's initial value.
*
*@author Nevena Marinkovic (nmarink1@jhu.edu)
*@since 2016-09-11
*/

public interface ResetableCounter extends Counter {
    @Override
    int value();

    @Override
    void up();
    
    @Override
    void down();

    /** Reset this counter to its initial value. */
    void reset();
}
