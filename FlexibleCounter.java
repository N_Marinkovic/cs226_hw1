/**
 *FlexibleCounter implements the ResetableCounter interface. Its
 *initial value and increment size are set in int constructor. It
 *increments and decrements using the chosen interval.
 *
 *@author Nevena Marinkovic (nmarink1@jhu.edu)
 *@since 2016-09-11
 */

public class FlexibleCounter implements ResetableCounter{
    /** Private integer variable storing the current value
     *of the counter, initialized to a chosen value in the
     *constructor. */
    private int value;
    /** Private variable storing the size of the increment,
     *initialized to a chosen value in the constructor. */
    private int increment;
    /** Private integer variable storing the starting value
     *of the counter, initialized to a chosen value in the
     *constructor. */
    private int startVal;

    /** Constructor for FlexibleCounter. takes two integers
     *as input. The input start initializes the starting
     *value of the counter, and the input i initializes the
     *increment for the counter. */
    public FlexibleCounter(int start, int i) {
	this.value = start;
	this.increment = i;
	this.startVal = start;
    }

    
    @Override
    public int value() {
	return this.value;
    }
    
    /** Increments value by the specified integer. */
    public void up() {
	this.value += increment;
    }

    /** Decrements value by the specified integer. */
    public void down() {
	this.value -= increment;
    }

    /** Resets value to the specified starting value. */
    public void reset() {
	this.value = this.startVal;
    }

    /** This is the main method containing assert statements
     *testing for teh methods in the FlexibleCounter class.  */
    public static void main(String[] args) {

	//Test case where both values are positive
	ResetableCounter c1 = new FlexibleCounter(3, 5);

	assert(c1.value() == 3);
	c1.up();
	c1.up();
	assert(c1.value() == 13);
	c1.down();
	assert(c1.value() == 8);
	c1.down();
	c1.down();
	assert(c1.value() == -2);
	c1.reset();
	assert(c1.value() == 3);

	//Test case when initial value is negative and increment is positive
	ResetableCounter c2 = new FlexibleCounter(-3, 5);

	assert(c2.value() == -3);
	c2.up();
	c2.up();
	assert(c2.value() == 7);
	c2.down();
	c2.down();
	c2.down();
	assert(c2.value() == -8);
	c2.reset();
	assert(c2.value() == -3);

    }
}
